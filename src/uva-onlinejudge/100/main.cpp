#include <cstdio>
#include <algorithm>

using namespace std;

bool Odd (int number);
int cyclelength (int number,int max);

int main(int argc, char **argv) {
	
	int i, j,t_i, t_j, cycle = 0;
	int num = 0;

	while (scanf("%d %d",&i,&j) != EOF){
		
		//Temporary variables, used for the case when we swap the variables
		t_i =i;
		t_j = j;

		if (i > j) swap(i,j);

		cycle = cyclelength(i,num);

		// Verify the large cycle lenght
		for (int count = i+1; count <= j; count++) {
				if(cycle < cyclelength(count,num))
					 cycle = cyclelength(count,num);
		}
		printf("%d %d %d\n",t_i,t_j,cycle);
	}

	return 0;
}

// Verify if the number is Odd or not
bool Odd (int number) {
	if ((number%2) == 0 ) return false;
	return true;
}

//Cyclelength algorithm is defined by the program specification
int cyclelength (int number, int max){
	
	if (number == 1) {
		++max;
		return max;
	}
	if (Odd(number)) {
		++max;
		cyclelength((3*number)+1, max);
	}
	else {
		++max;
		cyclelength(number/2,max);
	}
}