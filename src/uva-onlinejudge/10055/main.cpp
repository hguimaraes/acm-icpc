#include <cstdio>

int main(int argc, char **argv) {

	long long num_1,num_2,result;

	while (scanf("%lld %lld",&num_1,&num_2) != EOF){
		(num_1 < num_2) ? (result = num_2 - num_1):(result = num_1 - num_2);
		printf("%lld\n",result);
	}
	return 0;
}