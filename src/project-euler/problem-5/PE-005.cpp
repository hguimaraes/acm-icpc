#include <iostream>
#include <cstdlib>
#include <chrono>
#include <vector>

using namespace std;

long long int evenlydiv(long long int x){
	long long int num;
	long long int count = 1;
	vector<bool> array;
	while(1){
		for (int i = 1; i <= x; ++i){
			if (count % i == 0) array.push_back(false);	
		}
		if(array.size() == x){
		 	num = count;
			break;
		}
		array.clear();
        count++;
	}

	return num;
}

int main(int argc, char **argv){

	long long int N = atoll(argv[1]);
	cout << evenlydiv(N) << endl;
	return 0;
}