#include <iostream>
#include <cstdlib>
typedef long long int lld;

using namespace std;

lld sumPA (lld AN){
    return AN*(1+AN)/2;
}

lld sum2PA(lld AN){
    return AN*(AN+1)*(2*AN + 1)/6;
}

int main(int argc, char **argv){
	if (argc < 2) {
		cout << "Usage : Program.exe N" << endl;
	}

	// Collect data from command line
    lld N = atoll(argv[1]);
    
    // Square of the sum
	lld SquarePA = sumPA(N)*sumPA(N);
	
    // Sum of the squares
    lld Sum2 = sum2PA(N);
    
    if(SquarePA > Sum2) cout << SquarePA-Sum2 << endl;
    else cout << Sum2 - SquarePA << endl;
    
	return 0;
}

