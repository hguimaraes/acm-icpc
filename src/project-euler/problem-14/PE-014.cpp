#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;
typedef unsigned long long ull;

ull collatz(ull value, ull count){
    if(value == 1) {
        count++;
        return count;
    }
    
    else if(value%2 == 0) {
        count++;
        collatz(value/2,count);
    }
    
    else{
        count++;
        collatz(3*value + 1,count);
    }
}

ull solve(ull limit){
    
    ull large = 0;
    ull largenum = 0;
    ull result;
    
    for(int i = 1; i <= limit; i++){
        result = collatz(i,0);
        if (result > large) {
            largenum = i;
            large = result;
        }
    }
    
    return largenum;
}

int main(int argc, char **argv){
    ull n = atoll(argv[1]);
    cout << solve(n) << endl;
    return 0;
}