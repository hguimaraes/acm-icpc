/**/
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>

using namespace std;

vector<long long int> sieve(long long int N) {
  vector<long long int> primes;
  vector<bool> in(N, true);

  for (long long int i = 2; i <= sqrt(N); ++i) {
    if(in[i]) {
      for (long long int j = i * i; j <= N; j += i) {
        in[j] = false;
      }
    }
  }

  for(long long int i = 2; i <= N; ++i) {
    if(in[i]) {
      primes.push_back(i);
    }
  }

  return primes;
}

long long int solve(long long int N) {
    vector<long long int> primes = sieve((long long int)N*(log(N) + log(log(N))));
    cout << primes.size()<<endl;
    return primes[N-1];
}

int main(int argc, char **argv){
    long long int n = atoll(argv[1]); 
    cout << solve(n) << endl;
    return 0;
}