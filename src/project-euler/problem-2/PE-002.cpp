#include <iostream>
#include <vector>

#define max 4000000

using namespace std;

int main(int argc, char **argv){
	vector<int> fibo;
	fibo.push_back(1); //first term
	fibo.push_back(2); //second term
	long int sum = 2;
	int i = 2;
	
	while (fibo[i-1] <= max){
		if (fibo[i-2]+fibo[i-1] >= max) break;
		else {
            if ((fibo[i-2]+fibo[i-1])%2 == 0) sum += fibo[i-2]+fibo[i-1];
			fibo.push_back(fibo[i-2]+fibo[i-1]);
           i++;
		}
	}

	cout << "Result = " << sum << endl;
	return 0;
}