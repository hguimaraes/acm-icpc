#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>

using namespace std;

vector<long long int> sieve(long long int N) {
  vector<long long int> primes;
  vector<bool> in(N, true);

  for (long long int i = 2; i <= sqrt(N); ++i) {
    if(in[i]) {
      for (long long int j = i * i; j <= N; j += i) {
        in[j] = false;
      }
    }
  }

  for(long long int i = 2; i <= N; ++i) {
    if(in[i]) {
      primes.push_back(i);
    }
  }

  return primes;
}

long long int solve(long long int N) {
  vector<long long int> primes = sieve(N);
  int size = primes.size();
  long long int sum = 0;
    
  for (long long int i = 0; i < size; ++i) {
        sum += primes[i];     
  }
    
  return sum;
}

int main(int argc, char *argv[]) {
  
    long long int number = atoll(argv[1]);
    cout <<  solve(number) << endl;

  return 0;
}