#include <iostream>
#include <cstdlib>
#include <chrono>

using namespace std;
long int progressionSolve(long int n, long int p);

int main(int argc, char **argv){
    long int a,b, limits;
	
    if (argc < 4){
        cout << "Usage : Program.exe a b limit" << endl;
        return 1;  
    } 
    
    a = (long int)atoi(argv[1]);
	b = (long int)atoi(argv[2]);
	limits = (long int)atoi(argv[3]) - 1;
    
    auto start = std::chrono::high_resolution_clock::now();
    cout << progressionSolve(a,limits) + progressionSolve(b,limits) - progressionSolve(a*b,limits) << endl;
    auto finish = std::chrono::high_resolution_clock::now();
	cout <<"Time elapsed : " <<std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() << "ns" << endl;
    return 0;
}

long int progressionSolve(long int n,long int p){
    return n*(p/n)*((p/n)+1)/2;
}