#include <iostream>
#include <cstdlib>
#include <chrono>

using namespace std;
bool multiple(long int x, long int a, long int b);

int main(int argc, char **argv){
	
	long int a,b, limits;
	long int sum = 0;
	
	if (argc < 4) return 1;

	a = (long int)atoi(argv[1]);
	b = (long int)atoi(argv[2]);
	limits = (long int)atoi(argv[3]);
	
	auto start = std::chrono::high_resolution_clock::now();
		
	for(int i = 0; i < limits ; i++){
		if(multiple(i,a,b)) sum +=i;
	}
	
	cout << sum << endl;

	auto finish = std::chrono::high_resolution_clock::now();
	cout <<"Time elapsed : " <<std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() << "ns" << endl;
	return 0;
}

bool multiple(long int x, long int a, long int b){
	if(x%a == 0 || x%b == 0) return true;
	return false;
}