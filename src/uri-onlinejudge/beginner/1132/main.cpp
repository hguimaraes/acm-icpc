#include <iostream>
#include <algorithm>

using namespace std;

int main(int argc, char **argv){
	int x,y;
	int sum = 0;

	cin >> x >> y;
	if (x > y) swap(x,y);
	
	for (int i = x; i <= y; ++i){
		if ((i%13) != 0) sum +=i;
	}	
	cout << sum << endl;
	return 0;
}