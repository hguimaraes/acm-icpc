#include <cstdio>
#include <cmath>
#define QTD_OF_NOTES 7

void notes(int value, int *result);

int main(int argc, char **argv){
	int value;
	int *result;

	result = new int[QTD_OF_NOTES];
	scanf("%d",&value);
	notes(value,result);

	printf("%d\n",value);
	printf("%d nota(s) de R$ 100,00\n",result[0]);
	printf("%d nota(s) de R$ 50,00\n", result[1]);
	printf("%d nota(s) de R$ 20,00\n", result[2]);
	printf("%d nota(s) de R$ 10,00\n", result[3]);
	printf("%d nota(s) de R$ 5,00\n",  result[4]);
	printf("%d nota(s) de R$ 2,00\n",  result[5]);
	printf("%d nota(s) de R$ 1,00\n",  result[6]);

	delete []result;
	return 0;
}

void notes(int value, int *result){
	result[0] = trunc(value / 100);
	result[1] = trunc((value - 100*result[0])/50);
	result[2] = trunc((value - 100*result[0] - 50*result[1])/20);
	result[3] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2])/10);
	result[4] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3])/5);
	result[5] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3] - 5*result[4])/2);
	result[6] = trunc(value  - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3] - 5*result[4]- 2*result[5]);
}