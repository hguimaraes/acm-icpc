#include <iostream>
#include <cstring>
#include <cmath>
#define SIZE 100

using namespace std;

int main(int argc, char **argv){
	string *A = new string[SIZE];
	const double eps = 1e-9;
	
	for (int i = 0; i < SIZE; ++i){
		cin >> A[i];
		if (atof(A[i].c_str()) <= 10) {
			((fabs(atof(A[i].c_str())) - abs((int)atof(A[i].c_str()))) > eps) ? 
					cout << "A[" << i << "] = "<< A[i] <<endl :
					cout << "A[" << i << "] = "<< (int)atof(A[i].c_str()) <<endl;
		}
	}

	delete []A;
	return 0;
}