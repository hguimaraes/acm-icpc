#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
void printvector (vector<int>& x);
bool isOdd(int value);

int main(int argc, char **argv){
	int value;
	int countdown;
	int center;

	while (cin >> value){
		vector<int> array(value,3);

		array[0] = 1;
		array[value-1] = 2;
		printvector(array);
		center = value/2;

		for (int i = 0, countdown = value-1; i < value-1; ++i, --countdown){
			if(!isOdd(value)){
				if ((countdown - i) == 1) swap(array[i],array[i+1]);
				else {	
					swap(array[i],array[i+1]);
					swap(array[countdown],array[countdown-1]);
				}
			} else {
				if ((countdown - center) == 1 && (center - i) == 1){
					array[countdown] = 3;
					array[i] = 3;
					array[center] = 2;

				} else if(countdown == i) {
					array[countdown-1] = 2;
					array[i+1] = 1;
					array[center] = 3;

				} else {	 
					swap(array[i],array[i+1]);
					swap(array[countdown],array[countdown-1]);
				}
			}
			
			printvector(array);
		}

		array.clear();

	}
	return 0;
}

void printvector (vector<int>& x) {
	for (vector<int>::iterator it = x.begin();
		 it != x.end(); ++it){
		cout << *it;
	}

	cout << endl;
}

bool isOdd (int value) {
	if(value%2 == 0) return false;
	return true;
}