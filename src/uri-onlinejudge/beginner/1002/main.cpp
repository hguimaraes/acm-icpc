#include <cstdio>
#define PI 3.14159

int main(int argc, char **argv){
	double R,A;

	scanf("%lf",&R); //Read the Radius
	printf("A=%.4lf\n",PI*R*R); // Calculate and print the Area

	return 0;
}