#include <cstdio>

int main(int argc, char **argv){

	int N;
	double x,y;
	
	scanf("%d",&N);
	for (int i = 0; i < N; ++i){
		scanf("%lf%lf",&x,&y);
		if (y == 0) printf("divisao impossivel\n");
		else printf("%.1lf\n",x/y);
	}
	return 0;
}