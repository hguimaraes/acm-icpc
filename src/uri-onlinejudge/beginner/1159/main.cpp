#include <cstdio>

bool even(int value){
	if (value%2 == 0) return true;
	return false;
}

int main(int argc, char **argv){
	
	int Number = 1;
	int sum = 0;

	while (Number != 0){
		scanf("%d",&Number);
		if (Number == 0) break;
		sum = 0;

		if (even(Number)) {
			for (int i = Number; i <= (Number + 9); i+=2){
				sum += i;
			}

		} else {
			for (int i = Number+1; i <= Number + 10; i+=2){
				sum += i;
			}
		}

		printf("%d\n",sum); 
	}
	return 0;
}