#include <cstdio>
#define SIZE 100

int main(int argc, char **argv){

	double value;
	scanf("%lf",&value);

	for (int i = 0; i < SIZE; ++i){
		printf("N[%d] = %.4lf\n",i,value);
		value/= 2;	
	
	}

	return 0;
}