#include <cstdio>

int main(int argc, char **argv){

	int N;
	int *fibo = new int[46];
	scanf("%d",&N);
	for (int i = 0; i < N; ++i){
		if (i == 0) fibo[i] = 0;
		else if (i == 1 || i == 2) fibo[i] = 1;
		else fibo[i] = fibo[i-1]+fibo[i-2];

		if (i == (N-1)) printf("%d\n",fibo[i]);
		else printf("%d ",fibo[i]); 
	}
	return 0;
}