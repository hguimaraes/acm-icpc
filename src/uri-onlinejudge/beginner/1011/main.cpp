#include <cstdio>
#define PI 3.14159

int main(int argc, char **argv){
	int R;
	double vol;

	scanf("%d",&R);
	vol = (4.0/3)*PI*R*R*R;
	printf("VOLUME = %.3lf\n",vol);
	return 0;
}