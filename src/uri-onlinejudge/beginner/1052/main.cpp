#include <iostream>
#include <cstring>

using namespace std;

int main(int argc, char **argv){
	string months[] = {
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	};

	unsigned short num;
	cin >> num;
	cout << months[num-1]<<endl;

	return 0;
}