#include <cstdio>
#define NUM_OF_ENTERS 5

int even (int *enter);
int positive(int *enter);
int zeros(int *enter);

int main(int argc, char **argv){
	
	int *values = new int[NUM_OF_ENTERS];
	for (int i = 0; i < NUM_OF_ENTERS; ++i)
		scanf("%d",&values[i]);

	printf("%d valor(es) par(es)\n",even(values));
	printf("%d valor(es) impar(es)\n",NUM_OF_ENTERS - even(values));
	printf("%d valor(es) positivo(s)\n",positive(values));
	printf("%d valor(es) negativo(s)\n",NUM_OF_ENTERS - positive(values) - zeros(values));
	
	delete []values;
	return 0;
}

int even (int *enter){
	int result = 0;

	for (int i = 0; i < NUM_OF_ENTERS; ++i)
		if(enter[i]%2 == 0) result ++;
	
	return result;
}
int positive(int *enter){
	int result = 0;

	for (int i = 0; i < NUM_OF_ENTERS; ++i)
		if(enter[i] > 0) result ++;
	
	return result;
}

int zeros(int *enter) {
	int result = 0;

	for (int i = 0; i < NUM_OF_ENTERS; ++i)
		if(enter[i] == 0) result ++;

	return result;
}