#include <cstdio>
#include <cmath>
#include <iostream>

using namespace std;

#define QTD_OF_NOTES 7

void notes(double dvalue, int *result);
void coins(double dvalue, int *result);

int main(int argc, char **argv){
	double dvalue;
	int *result;

	result = new int[QTD_OF_NOTES];
	scanf("%lf",&dvalue);
	notes(dvalue,result);

	printf("NOTAS:\n");
	printf("%d nota(s) de R$ 100.00\n",result[0]);
	printf("%d nota(s) de R$ 50.00\n", result[1]);
	printf("%d nota(s) de R$ 20.00\n", result[2]);
	printf("%d nota(s) de R$ 10.00\n", result[3]);
	printf("%d nota(s) de R$ 5.00\n",  result[4]);
	printf("%d nota(s) de R$ 2.00\n",  result[5]);
		
	coins(dvalue,result);

	printf("MOEDAS:\n");
	printf("%d moeda(s) de R$ 1.00\n", result[6]);
	printf("%d moeda(s) de R$ 0.50\n", result[0]);
	printf("%d moeda(s) de R$ 0.25\n", result[1]);
	printf("%d moeda(s) de R$ 0.10\n", result[2]);
	printf("%d moeda(s) de R$ 0.05\n", result[3]);
	printf("%d moeda(s) de R$ 0.01\n", result[4]);

	delete []result;
	return 0;
}

void notes(double dvalue, int *result){

	int value = trunc(dvalue);
	result[0] = trunc(value / 100);
	result[1] = trunc((value - 100*result[0])/50);
	result[2] = trunc((value - 100*result[0] - 50*result[1])/20);
	result[3] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2])/10);
	result[4] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3])/5);
	result[5] = trunc((value - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3] - 5*result[4])/2);
	result[6] = trunc(value  - 100*result[0] - 50*result[1] - 20*result[2] - 10*result[3] - 5*result[4]- 2*result[5]);
}

void coins(double dvalue, int *result){
	double value = dvalue - trunc(dvalue);
	result[0] = trunc(value / 0.5);
	result[1] = trunc((value - 0.5*result[0])/0.25);
	result[2] = trunc((value - 0.5*result[0] - 0.25*result[1])/0.1);
	result[3] = trunc((value - 0.5*result[0] - 0.25*result[1] - 0.1*result[2])/0.05);
	result[4] = trunc((value - 0.5*result[0] - 0.25*result[1] - 0.1*result[2] - 0.05*result[3])/0.01);
}