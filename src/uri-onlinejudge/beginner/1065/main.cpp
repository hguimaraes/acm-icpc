#include <cstdio>
#define NUMBER_OF_ENTERS 5

bool even (int value) {
	if ((value%2) == 0) return true;
	else return false;
}

int main(int argc, char **argv){
	int enter,num = 0;
	for (int i = 0; i < NUMBER_OF_ENTERS; ++i){
			scanf("%d",&enter);
			if (even(enter)) num++;
	}
	printf("%d valores pares\n",num);

	return 0;
}