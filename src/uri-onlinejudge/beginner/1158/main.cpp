#include <cstdio>

bool odd(int Number){
	if (Number%2 == 0) return false;
	return true;
}

int main(int argc, char **argv){

	int N,sum,stop;
	int X,Y;

	scanf("%d",&N);
	for (int i = 0; i < N; ++i){
		scanf("%d%d",&X,&Y);
		sum = 0;
		stop = 0;
		
		for (int j = X; ; j++){
			if (odd(j)){ 
				sum += j;
				stop++;
			}
			if (stop ==Y) break;
			
		}
		 
		printf("%d\n",sum);
	}
	return 0;
}