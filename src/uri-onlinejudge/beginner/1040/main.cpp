#include <cstdio>
#define NUMBER_OF_GRADES 5

void media(double *notes);

int main(int argc, char **argv){
	
	double *grade;
	grade = new double[NUMBER_OF_GRADES];

	for (int i = 0; i < NUMBER_OF_GRADES - 1; ++i)
		scanf("%lf",&grade[i]);

	media(grade);

	delete [] grade;
	return 0;
}

void media(double *grade){

	double media = (2*grade[0] + 3*grade[1] + 4*grade[2] + grade[3])/10;
	printf("Media: %.1lf\n",media);
	if (media < 5) printf("Aluno reprovado.\n");
	else if (media >= 7) printf("Aluno aprovado.\n");
	else {
		printf("Aluno em exame.\n");
		scanf("%lf",&grade[4]);
		printf("Nota do exame: %.1lf\n",grade[4]);
		media = (media + grade[4]) /2;
		(media >= 5) ? printf("Aluno aprovado.\n") : printf("Aluno reprovado.\n");
		printf("Media final: %.1lf\n",media);
	}
}