#include <iostream>
#include <cstdio>

#define SIZE 144
#define SIZELINE 12
#define SIZEROW 12

using namespace std;

int main(int argc, char **argv)
{
	int divi = 0;
	char operation;
	float elements [SIZE];
	float sum = 0;
	
	cin >> operation;

	for (int i = 0; i < SIZE; ++i)
		cin >> elements[i];

	for (int i = 0; i < SIZELINE; ++i){
		for (int j = 0; j < SIZEROW - (i+1); ++j){
				sum += elements[i*SIZELINE + j];
				divi++;
			}
	}

	if (operation == 'S') printf("%.1f\n",sum);
	else printf("%.1f\n",sum/divi);
	return 0;
}