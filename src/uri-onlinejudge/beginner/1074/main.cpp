#include <cstdio>

int main(int argc, char **argv){

	int N,value;
	scanf("%d",&N);

	for (int i = 0; i < N; ++i){
		scanf("%d",&value);
		if ((value%2) == 0) {
			if (value > 0) printf("EVEN POSITIVE\n");
			else if (value == 0) printf("NULL\n");
			else printf("EVEN NEGATIVE\n");

		}
		else {
			if (value > 0) printf("ODD POSITIVE\n");
			else printf("ODD NEGATIVE\n");
		}	
	}

	return 0;
}