#include <cstdio>

int main(int argc, char **argv){

	int N;
	double values[2];
	scanf("%d",&N);

	for (int i = 0; i < N; ++i){
		
		for (int j = 0; j < 3; ++j)
			scanf("%lf",&values[j]);


		printf("%.1lf\n",((values[0]*2) + (values[1]*3) + (values[2]*5))/10);
	}
	return 0;
}