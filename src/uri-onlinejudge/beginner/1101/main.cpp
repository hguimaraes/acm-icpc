#include <iostream>
#include <algorithm>

using namespace std;

int main(int argc, char **argv){

	int min = 1, max = 1;
	int sum = 0;
	while (min > 0 && max > 0) {
		cin >> min >> max;
		if (min <= 0 || max <= 0) break;

		sum = 0;
		if (max < min) swap(min,max);
		
		for (int i = min; i <= max; ++i){
			cout << i <<" " ;
			sum += i;
		}

		cout <<"Sum="<<sum << endl;

	}
	return 0;
}