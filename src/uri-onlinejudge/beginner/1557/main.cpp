#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

/* matrix [n][m]
 n = linha
 m = coluna 
 linearização : matrix[n*m] == matrix[i*m + j]
 0 <= i <= n-1 && 0 <= j <= m-1*/

int main(int argc, char **argv){
	int value = 1;
	int att = 1;
	
	while (cin >> value && value != 0){
		
		vector< vector<int> > matrix(value, vector<int>(value));
			for (int i = 0; i < value; ++i){
				for (int j = i; j < value; ++j){
					
					if (i == 0 && j == 0) matrix[i][j] = 1;
					else if (i == j) matrix[i][j] = matrix[i-1][j-1]*4;
					else {
						matrix[i][j] = matrix[i][j-1]*2;
						matrix[j][i] = matrix[j-1][i]*2;
					}

				}				
			}

		int biggest = matrix[value-1][value-1];
		int count = 0;

        while(biggest > 0){
         	biggest /= 10;
         	count++;
      	}
      
     // Printing
      for (int i = 0; i < value; ++i){
			for (int j = 0; j < value; ++j){
            	int actual = matrix[i][j];
            	int cont = 0;
            	int k; 
            
            while(actual > 0){
               actual /= 10;
               cont++;
            }
            
            if( j > 0){
               for(k = 0; k < count+1 - cont; k++) cout << " "; 
            } else {
               for(k = 0; k < count - cont; k++) cout << " ";
                
            }
            
            printf("%d", matrix[i][j]);
            
         }
         cout << endl;
      }
      
   		cout << endl;
		matrix.clear();
	}

	return 0;
}