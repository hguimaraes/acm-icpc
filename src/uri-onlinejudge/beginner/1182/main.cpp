#include <iostream>
#include <cstdio>

#define SIZE 144
#define SIZELINE 12
#define SIZEROW 12

using namespace std;

int main(int argc, char **argv)
{
	int row;
	char operation;
	float elements [SIZE];
	float sum = 0;
	float average;

	cin >> row >> operation;

	for (int i = 0; i < SIZE; ++i)
		cin >> elements[i];

	for (int i = 0; i < SIZEROW; ++i)
		sum += elements[i*SIZELINE + row];

	if (operation == 'S') printf("%.1f\n",sum);
	else printf("%.1f\n",sum/SIZELINE);
	return 0;
}