#include <iostream>
#include <cstring>
#define NUMBER_OF_WORDS 3

using namespace std;

void classify(string *word);

int main(int argc, char **argv){

	string *enter;
	enter = new string[NUMBER_OF_WORDS];

	for (int i = 0; i < NUMBER_OF_WORDS; ++i)
		cin >> enter[i];

	classify(enter);

	delete []enter;
	return 0;
}

void classify(string *word){

	if (word[0] == "invertebrado") {
		if (word[1] == "inseto"){
			if (word[2] == "hematofago") printf("pulga\n");
			else printf("lagarta\n");
		}
		else {
			if (word[2] == "hematofago") printf("sanguessuga\n");
			else printf("minhoca\n");
		}
	}

	else {
		if (word[1] == "ave"){
			if (word[2] == "carnivoro") printf("aguia\n");
			else printf("pomba\n");
		}
		else {
			if (word[2] == "onivoro") printf("homem\n");
			else printf("vaca\n");
		}
	}
}