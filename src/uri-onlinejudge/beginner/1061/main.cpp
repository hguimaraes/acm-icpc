#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#define NUM_OF_LINES 4
#define NUM_OF_VARIABLES 4
/*Consider the following rule for my integers arrays :
Position 0 - Day
Position 1 - Hour
Position 2 - Minute
Position 3 - Second */

using namespace std;

class data {

		int *initVALUES   = new int[NUM_OF_VARIABLES];
		int *finalVALUES  = new int[NUM_OF_VARIABLES];
		int *resultVALUES = new int[NUM_OF_VARIABLES];
		
		void calculateResult(int*, int*, int*);

	public:
		void setValue(int*, int*);
		void getResult ();
};

void data::setValue(int *initValues, int *finalValues){
	initVALUES  = initValues;
	finalVALUES = finalValues;
}

void data::calculateResult(int *init, int *final, int *result){
	
	long long int auxiliar_start, auxiliar_end, diff;
	
	auxiliar_start = (init[0]*24*60*60) +
					 (init[1]*60*60)    +
					 (init[2]*60)       +
					 (init[3]);

	auxiliar_end =	(final[0]*24*60*60) +
					(final[1]*60*60)    +
					(final[2]*60)       +
					(final[3]);		

	diff = auxiliar_end - auxiliar_start;				
	result[0] = trunc(diff/(24*60*60));
	result[1] = trunc((diff - (24*60*60)*result[0])/(60*60));
	result[2] = trunc((diff - (24*60*60)*result[0] - (60*60)*result[1])/60);
	result[3] = trunc((diff - (24*60*60)*result[0] - (60*60)*result[1] - 60*result[2]));				
	 
}

void data::getResult(){
		calculateResult(initVALUES,finalVALUES,resultVALUES);
		cout << resultVALUES[0] << " dia(s)"     << endl;
		cout << resultVALUES[1] << " hora(s)"    << endl;
		cout << resultVALUES[2] << " minuto(s)"  << endl;
		cout << resultVALUES[3] << " segundo(s)" << endl;
}

int main(int argc, char **argv){

	string *line = new string[NUM_OF_LINES];;
	string garbit,day[2];


	// Interger pointer for start event
	int *startEve = new int[NUM_OF_VARIABLES];
	
	//Integer pointer for end event
	int *endEve = new int[NUM_OF_VARIABLES];
	
	// Read the lines from enter file
	for (int i = 0; i < NUM_OF_LINES; ++i)
		getline(cin,line[i]);
		
	
	startEve[0] = atoi(line[0].erase(0,4).c_str());
	startEve[1] = atoi(line[1].substr(0,2).c_str());
	startEve[2] = atoi(line[1].substr(5,2).c_str());
	startEve[3] = atoi(line[1].substr(10,2).c_str());

	endEve[0] = atoi(line[2].erase(0,4).c_str());
	endEve[1] = atoi(line[3].substr(0,2).c_str());
	endEve[2] = atoi(line[3].substr(5,2).c_str());
	endEve[3] = atoi(line[3].substr(10,2).c_str());

	data enter;
	enter.setValue(startEve,endEve);
	enter.getResult();

	delete []line;
	delete []startEve;
	delete []endEve;

	return 0;
}