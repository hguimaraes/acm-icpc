#include <cstdio>
#define NUMBER_OF_ENTERS 6

int main(int argc, char **argv){
	int i = 0;
	int result = 0;
	double number;

	while (i < NUMBER_OF_ENTERS) {
		scanf("%lf",&number);
		if (number > 0)
			result++;
		i++;
	}

	printf("%d valores positivos\n",result);
	return 0;
}