#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

/* matrix [n][m]
 n = linha
 m = coluna 
 linearização : matrix[n*m] == matrix[i*m + j]
 0 <= i <= n-1 && 0 <= j <= m-1*/

int main(int argc, char **argv){
	int value = 1;
	int att = 1;
	
	while (cin >> value && value != 0){
		
		vector< vector<int> > matrix(value, vector<int>(value));
			for (int i = 0; i < value; ++i){
				for (int j = i; j < value; ++j){
					if (i == j) matrix[i][j] = 1;
					else if(matrix[i][j] == 0 && matrix[j][i] ==0){
						matrix[i][j] = att;
						matrix[j][i] = att;
					}
					att++;
				}
				att = 1;
			}


		for (int i = 0; i < value; ++i){
			for (int j = 0; j < value; ++j){
				if (j != value-1) printf("%3d ",matrix[i][j]);
				else printf("%3d",matrix[i][j]);
			}
			
			cout << endl;
		}
		
		cout << endl;
		matrix.clear();
	}

	return 0;
}