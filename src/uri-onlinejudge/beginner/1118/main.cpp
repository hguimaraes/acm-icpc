#include <cstdio>

int main(int argc, char **argv){
	int counter = 0, option = 0;
	double notas[2];
	double enter;

	while (option != 2) {
		while (counter != 2) {
			scanf("%lf",&enter);
			if (enter < 0 || enter > 10) printf("nota invalida\n");
			else {
				notas[counter] = enter;
				counter ++;
			}

		}

		printf("media = %.2lf\n",(notas[0]+notas[1]) / 2);
		printf("novo calculo (1-sim 2-nao)\n");
		counter = 0;

		while(option != 2){
			scanf("%d",&option);
			if (option == 1 || option == 2) break;
			else printf("novo calculo (1-sim 2-nao)\n");
		}
	}

	return 0;
}