#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

/* matrix [n][m]
 n = linha
 m = coluna 
 linearização : matrix[n*m] == matrix[i*m + j]
 0 <= i <= n-1 && 0 <= j <= m-1*/

bool isOdd(int value){
	if (value%2 == 0) return false;
	return true;
}

int main(int argc, char **argv){
	int value = 1;
	
	while (cin >> value && value != 0){
		vector<int> matrix(value*value,1);

		if(isOdd(value)) matrix[value*value/2] = (value/2)+1; 		
		for (int i = 1; i < value/2; ++i){
			for (int j = i; j < value - i; ++j){
				matrix[i*value + j] = i+1;
				matrix[j*value + i] = i+1;

				matrix[(value-(i+1))*value + j] = i+1;
				matrix[j*value + (value-(i+1))] = i+1;
			}
		}

		for (int i = 0; i < value; ++i){
			for (int j = 0; j < value; ++j){
				if (j != value-1) printf("%3d ",matrix[i*value + j]);
				else printf("%3d",matrix[i*value + j]);
			}
			
			cout << endl;
		}
		
		cout << endl;
		matrix.clear();
	}

	return 0;
}