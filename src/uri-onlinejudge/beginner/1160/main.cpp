#include <iostream>
#include <cmath>

using namespace std;

void timeToGrow(int count, long int A, long int B, double GA, double GB);
int main(int argc, char **argv){
	int T, count = 0;
	long int PA,PB;
	double GA,GB;

	cin >> T;

	for (int i = 0; i < T; ++i){
		cin >> PA >> PB >> GA >> GB;
		timeToGrow(count,PA,PB,GA,GB);	
	}

	return 0;
}


void timeToGrow(int count, long int A, long int B, double GA, double GB){
	
	A = floor(A*(1 + (GA/100)));
	B = floor(B*(1 + (GB/100)));
	count++;
	
	if(count > 100) cout << "Mais de 1 seculo."<< endl;
	else if(A > B) cout << count << " anos." << endl;
	else timeToGrow(count,(long int)A,(long int)B,GA,GB);
	
}