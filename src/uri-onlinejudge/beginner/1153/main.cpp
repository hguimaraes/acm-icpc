#include <cstdio>

int factorial(int value){
	int fat;

	if(value <= 1) return 1;
	fat = value*factorial(value-1);

	return fat;
}

int main(){
	int N, x;
	scanf("%d",&N);
	printf("%d\n",factorial(N));
	return 0;
}