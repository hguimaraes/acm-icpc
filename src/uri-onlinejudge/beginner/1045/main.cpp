#include <cstdio>
#include <algorithm>

using namespace std;

int main(int argc, char **argv){
	
	double A,B,C;
	double greatest;

	scanf("%lf",&A);
	greatest = A;

	scanf("%lf",&B);
	if (B > greatest) {
		greatest = B;
		swap(A,B);
	}

	scanf("%lf",&C);
	if (C > greatest) swap(A,C);

	if (A >= B + C) printf("NAO FORMA TRIANGULO\n");
	else {
			if ((A*A) == ((B*B) + (C*C))) printf("TRIANGULO RETANGULO\n");
			if ((A*A) > ((B*B) + (C*C)))  printf("TRIANGULO OBTUSANGULO\n");
			if ((A*A) < ((B*B) + (C*C)))  printf("TRIANGULO ACUTANGULO\n");
			
			if ((A == B) && (B == C) && (A == C)) printf("TRIANGULO EQUILATERO\n");
			else if ((A == B) || (B == C) || (A == C)) printf("TRIANGULO ISOSCELES\n");
	}

	return 0;
}