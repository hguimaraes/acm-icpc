#include <iostream>
#include <vector>

#define SIZE 15

using namespace std;

bool Odd(int number){
	if (number% 2 == 0) return false;
	return true;
}

int main(int argc, char **argv){

	
	int value;
	vector<int> odd;
	vector<int> even;
	
	for (int i = 0; i < SIZE; ++i){
		
		cin >> value;
		
		if (Odd(value)) {
			odd.push_back(value);
			
			if ((odd.size() % 5) == 0) {
				for (vector<int>::iterator it = odd.begin(); it != odd.end(); ++it)
					cout << "impar["<< it - odd.begin() << "] = "<< *it << endl;
				odd.clear();
			}	
				
		} else {
			even.push_back(value);

			if ((even.size() % 5) == 0) {
				for (vector<int>::iterator it = even.begin(); it != even.end(); ++it)
					cout << "par["<< it - even.begin() << "] = "<< *it << endl;
				even.clear();
			}
		} 
		
	}

	for (vector<int>::iterator it = odd.begin(); it != odd.end(); ++it)
			cout << "impar["<< it - odd.begin() << "] = "<< *it << endl;

	for (vector<int>::iterator it = even.begin(); it != even.end(); ++it)
			cout << "par["<< it - even.begin() << "] = "<< *it << endl;

	return 0;
}