#include <cstdio>
#define NUM_OF_ENTERS 6

int main(int argc, char **argv){

	double enter;
	double average = 0;
	int positive = 0;

	for (int i = 0; i < NUM_OF_ENTERS; ++i){
		scanf("%lf",&enter);
		if(enter > 0) {
			positive ++;
			average += enter;
		}
	}

	average /= positive;

	printf("%d valores positivos\n",positive);
	printf("%.1lf\n",average);
	return 0;
}