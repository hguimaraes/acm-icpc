#include <cstdio>

int main(){
	long double sum = 0;
	for (int i = 1; i <= 100; ++i){
		sum += (long double) 1.0/i;
	}
	printf ("%.2llf\n",sum);
	return 0;
}