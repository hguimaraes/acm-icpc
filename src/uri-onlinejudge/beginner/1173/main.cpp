#include <cstdio>
#define SIZE 10

int main(int argc, char **argv){
	int *x = new int[SIZE];
	int value;
	scanf("%d",&value);
	x[0] = value;
	for (int i = 0; i < SIZE; ++i){
		if (i == 0) {
			x[i] = value;
			printf("N[%d] = %d\n",i,x[i]);
			continue;
		}
		value = value << 1 ;
		x[i] = value;
		printf("N[%d] = %d\n",i,x[i]);
	}
	
	return 0;
}