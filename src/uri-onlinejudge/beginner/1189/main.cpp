#include <iostream>
#include <cstdio>

#define SIZE 144
#define SIZELINE 12
#define SIZEROW 12

using namespace std;

int main(int argc, char **argv)
{
	int divi = 0;
	char operation;
	double elements [SIZE];
	double sum = 0;
	
	cin >> operation;

	for (int i = 0; i < SIZE; ++i)
		cin >> elements[i];

	for (int i = 0; i < SIZEROW/2; ++i){
		for (int j = SIZELINE*(i+1); j < SIZELINE*(SIZELINE-(i+1)); j+=SIZELINE){
				sum += elements[i+j];
				divi++;				
		}
	}

	if (operation == 'S') printf("%.1lf\n",sum);
	else printf("%.1lf\n",sum/divi);
	return 0;
}