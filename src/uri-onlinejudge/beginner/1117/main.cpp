#include <cstdio>

int main(int argc, char **argv){
	int counter = 0;
	double notas[2];
	double enter;

	while (counter != 2) {
		scanf("%lf",&enter);
		if (enter < 0 || enter > 10) printf("nota invalida\n");
		else {
			notas[counter] = enter;
			counter ++;
		}

	}
	printf("media = %.2lf\n",(notas[0]+notas[1]) / 2);

	return 0;
}