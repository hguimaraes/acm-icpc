#include <iostream>
#include <cstdio>

#define SIZE 144
#define SIZELINE 12
#define SIZEROW 12

using namespace std;

/* matrix [n][m]
 n = linha
 m = coluna 
 linearização : matrix[n*m] == matrix[i*m + j]
 0 <= i <= n-1 && 0 <= j <= m-1*/

int main(int argc, char **argv)
{
	int divi = 0;
	char operation;
	double elements [SIZE];
	double sum = 0;
	
	cin >> operation;

	for (int i = 0; i < SIZE; ++i)
		cin >> elements[i];

	for (int i = SIZEROW/2; i < SIZEROW; ++i){
		for (int j = SIZELINE-i; j < i; ++j){
				sum += elements[i+j*SIZELINE];
				divi++;
				elements[i+j*SIZELINE] = 0;
		}
	}

	for (int i = 0; i < SIZEROW; ++i){
		for (int j = 0; j < SIZELINE; ++j){
				cout << elements[i*SIZEROW + j] << " ";
		}
		cout << endl;
	}

	if (operation == 'S') printf("%.1lf\n",sum);
	else printf("%.1lf\n",sum/divi);
	return 0;
}