#include <cstdio>
#include <cmath>

int main(int argc, char **argv){
	
	int stime;
	int hour,minutes, seconds;
	scanf("%d",&stime);

	hour    = trunc(stime/3600);
	minutes = trunc((stime - hour*3600)/60);
	seconds = trunc(stime - hour*3600 - 60*minutes); 
	
	printf("%d:%d:%d\n",hour, minutes, seconds);
	return 0;
}