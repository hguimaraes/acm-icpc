#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int main(int argc, char **argv){
	string NAME;
	double SALARY, SOLD , COMMISSION;

	cin >> NAME >> SALARY >> SOLD;
	COMMISSION = 0.15*SOLD;
	
	printf("TOTAL = R$ %.2lf\n", SALARY + COMMISSION);

	return 0;
}