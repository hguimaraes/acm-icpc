#include <cstdio>
#include <cmath>

class points{
	public:
		int x;
		int y;
};

int main(int argc, char **argv){

	points p1,p2;
	double result;

	scanf("%d%d",&p1.x,&p1.y);
	scanf("%d%d",&p2.x,&p2.y);

	result = sqrt(((p1.x-p2.x)*(p1.x-p2.x)) + ((p1.y-p2.y)*(p1.y-p2.y)));
	printf("%.4lf\n",result);

	return 0;
}