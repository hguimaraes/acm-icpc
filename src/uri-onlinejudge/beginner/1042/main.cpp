#include <cstdio>
#define Number_Of_Elements 3

void insertionsort (int *array);

int main(int argc, char **argv){
	
	int *vector1,vector2[Number_Of_Elements];
	vector1 = new int[Number_Of_Elements];
		
	for (int i = 0; i < Number_Of_Elements; ++i) {
		scanf("%d",&vector1[i]);
		vector2[i] = vector1[i];
	}

	insertionsort(vector1);
	
	printf("\n");
	for (int i = 0; i < Number_Of_Elements; i++)
		printf("%d\n",vector2[i]);

	return 0;
}

void insertionsort (int *array) {
	
	int key,j;
	
	// Insertion sort
	for (int i = 1; i < Number_Of_Elements; i++) {
		key = array[i];
		j = i-1;

		while (j >= 0 && array[j] > key) {
			array[j+1] = array[j];
			j--;
		}

		array[j+1] = key;
	}

	for (int i = 0; i < Number_Of_Elements; i++){
		printf("%d\n",array[i]);
	}

}