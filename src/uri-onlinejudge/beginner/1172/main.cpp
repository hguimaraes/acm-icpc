#include <cstdio>
#define SIZE 10

int main(int argc, char **argv){
	int *x = new int[SIZE];
	for (int i = 0; i < SIZE; ++i){
		scanf("%d",&x[i]);
		if(x[i] <= 0) x[i] = 1;
		printf("X[%d] = %d\n",i,x[i]);
	}
	return 0;
}