#include <cstdio>
#define SIZE 61

void Fibonacciarray (long long int *Array);

int main(int argc, char **argv){
	int N,value;
	
	long long int *Fibo = new long long int[SIZE];
	Fibonacciarray(Fibo);

	scanf("%d",&N);
	for (int i = 0; i < N; ++i){
		scanf("%d",&value);
		printf("Fib(%d) = %lld\n",value,Fibo[value]);
	}
	return 0;
}

void Fibonacciarray (long long int *Array){
	Array[0] = 0;
	Array[1] = 1;

	for (int i = 2; i < SIZE; ++i)
		Array[i] = Array[i-1] + Array[i-2];

}