#include <cstdio>

int main(int argc, char **argv){
	
	int ini_J = 7;
	
	int k = 0;
	int j = ini_J;
	int n = 1;

	for (int i = 1; i <= 9; i+=2){
		while (k != 3){
			printf("I=%d J=%d\n",i,j);
			j--;
			k++;
		}
		j = ini_J + 2*n;
		k = 0;
		n++;
	}
	return 0;
}