#include <cstdio>

int main(int argc, char **argv){
	int N = 1;
	while(N != 0) {
		scanf("%d",&N);
		
		if (N == 0) break;
		for (int i = 1; i <= N; ++i){
			if (i == N) printf ("%d",i);
			else printf ("%d ",i);
		}

		printf("\n");
	}

	return 0;
}