#include <iostream>
#include <algorithm>

using namespace std;

void oddbetween(int a, int b){

	int sum = 0;

	if ((a%2) == 0) {

		for (int i = a+1; i < b;i+= 2)
			sum +=i;
		
	}

	else {
		for (int i = a+2; i < b ;i+= 2)
			sum +=i;
		
	}

	cout << sum << endl;
}

int main(int argc, char **argv){
	int min, max;
	cin >> min >> max;

	if (min > max) swap(min,max);
	oddbetween(min,max);

	return 0;
}