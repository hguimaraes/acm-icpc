#include <cstdio>

int main(int argc, char **argv){
	
	double salary;
	scanf("%lf", &salary);

	if(salary <= 2000.00) 
		printf("Isento\n");

	if (salary > 2000.00 && salary <= 3000.00) 
		printf ("R$ %.2lf\n", (salary - 2000)*0.08);
	

	if (salary > 3000.00 && salary <= 4500.00)
		printf ("R$ %.2lf\n", (1000*0.08) + ((salary - 3000)*0.18));
	

	if (salary > 4500.00)
		printf ("R$ %.2lf\n", (1000*0.08) + (1500*0.18)+ ((salary-4500)*0.28));
	
	return 0;
}