#include <cstdio>
#include <cstdlib>
/*Before we solve this problem, there is another simple way of solve this just using if statement
but the problem ask us to solve using the given formula */

int main(int argc, char **argv){

	int A,B,C, greatest;
	int formula;
	scanf("%d%d%d",&A,&B,&C);

	formula = (A + B + abs(A-B)) / 2;
	greatest = formula;

	formula = (B + C + abs(B-C)) / 2;
	if(greatest < formula) greatest = formula;

	formula = (A + C + abs(A-C)) / 2;
	(formula > greatest) ? printf ("%d eh o maior\n",formula) : printf("%d eh o maior\n",greatest);
	return 0;
}