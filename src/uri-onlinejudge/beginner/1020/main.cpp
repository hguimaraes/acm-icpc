#include <cstdio>
#include <cmath>

int main(int argc, char **argv){

	int recvdays;
	int years, month,days;
	scanf("%d",&recvdays);

	years = trunc(recvdays/365);
	month = trunc((recvdays - 365*years)/30);
	days =  trunc( recvdays - 365*years - 30*month);

	printf("%d ano(s)\n" ,years );
	printf("%d mes(es)\n",month );
	printf("%d dia(s)\n" ,days  );
	return 0;
}