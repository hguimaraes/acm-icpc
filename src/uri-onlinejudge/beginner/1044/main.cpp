#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

int main(int argc, char **argv){
	int a,b;
	double mult;

	scanf("%d%d",&a,&b);
	if (b > a) swap(a,b);

	mult = ((double)a/b - trunc((double)a/b));

	(mult == 0) ? printf("Sao Multiplos\n") : printf("Nao sao Multiplos\n");

	return 0;
}