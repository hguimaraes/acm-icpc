#include <iostream>
#include <algorithm>

using namespace std;

bool checkDiagonal(pair<int,int>& queen, pair<int,int>& point);
bool checkQueue(pair<int,int>& queen, pair<int,int>& point);

int main(int argc, char **argv){

	pair<int,int> coordQueen;
	pair<int,int> coordPoint;

	while (cin >> coordQueen.first >> coordQueen.second >> 
		   coordPoint.first >> coordPoint.second &&
		   coordQueen.first!= 0 && coordQueen.second!= 0 &&
		   coordPoint.first!= 0 && coordPoint.second!= 0)
	{
		if (coordPoint == coordQueen) cout << "0" << endl;
		else if(checkDiagonal(coordQueen,coordPoint) || 
			    checkQueue(coordQueen,coordPoint)) cout << "1" << endl;
		else cout << "2" << endl;
	}

	return 0;
}

bool checkDiagonal(pair<int,int>& queen, pair<int,int>& point){
	if (queen.first > point.first) 	 swap(queen.first,point.first);
	if (queen.second > point.second) swap(queen.second, point.second);
	if ((point.first - queen.first) == (point.second - queen.second)) return true;
	return false;
}

bool checkQueue(pair<int,int>& queen, pair<int,int>& point){
	if (queen.first == point.first || queen.second == point.second) return true;
	return false;
}