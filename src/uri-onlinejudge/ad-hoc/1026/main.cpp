#include <iostream>
#include <cstring>
#include <bitset>

#define NUMBITS 32

using namespace std;
int notToCarry(string numA, string numB);

int main(int argc, char **argv){
	int A,B;
	string nbinA,nbinB;
	while (cin >> A >> B){
		nbinA = std::bitset< 32 >(A).to_string();
		nbinB = std::bitset< 32 >(B).to_string();

		//cout << notToCarry(nbinA,nbinB) << endl;

		cout << nbinA[31] << endl;
		cout << nbinB[31] << endl;
	}
	return 0;
}

int notToCarry(string numA, string numB){
	int result = 0;
	string partialResult;

	for (int i = 0; i < NUMBITS; ++i){
		if (numA[i] == numB[i]) partialResult[i] = "0";
		else partialResult[i] = "1";
	}

	//cout << partialResult << endl;

	return result;
}