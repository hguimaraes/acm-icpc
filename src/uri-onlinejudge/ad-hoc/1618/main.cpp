#include <cstdio>

int main(int argc, char **argv){
	int N;
	int *coord = new int[10];
	int minx = 0, maxx = 0;
	int miny = 0, maxy = 0;

	scanf("%d",&N);
	
	for (int i = 0; i < N; ++i){
		
		for(int j = 0; j < 8 ; j++){
			scanf("%d",&coord[j]);
			
			if (j == 0){
				minx = coord[j];
				maxx = coord[j];
			}
			if (j == 1){
				miny = coord[j];
				maxy = coord[j];
			}

			if ( (j%2) == 0) {
				if (coord[j] < minx) minx = coord[j];
				if (coord[j] > maxx) maxx = coord[j];
			}
			else {
				if (coord[j] < miny) miny = coord[j];
				if (coord[j] > maxy) maxy = coord[j];
			}
		}

		scanf("%d",&coord[8]);
		scanf("%d",&coord[9]);

		
			(coord[8] < minx || coord[8] > maxx || coord[9] < miny || coord[9] > maxy) ?
			printf("0\n") : printf("1\n");
				
				
	}
	
	return 0;
}