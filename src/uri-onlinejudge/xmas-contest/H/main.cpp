#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

class Rena {
	public:
		string name;
		int weight,age; 
		float height;

		struct compareMyDataFunctor : public binary_function<Rena, Rena, bool>{
      		bool operator()( Rena lhs, Rena rhs){
            	if (lhs.weight != rhs.weight)return (lhs.weight < rhs.weight);
				else if (lhs.age != rhs.age) return (lhs.age < rhs.age);
				else {
					if(lhs.name.compare(rhs.name) > 1) return true;
					else return false;
				}
         }
    };


};

int main(int argc, char **argv){
	int testNumber;
	int N,compare;
	int k = 0;
	cin >> testNumber;
	vector<Rena> animal;

	Rena mydata;
	for (int i = 0; i < testNumber; ++i){
		cin >> N >> compare;
		cout << "CENARIO {" << i+1 << "}" << endl;
		for (int j = 0; j < N; ++j){
			cin >> mydata.name >> mydata.weight >> mydata.age >> mydata.height;
			animal.push_back(mydata);
		}

		std::sort(animal.begin(), animal.end(), Rena::compareMyDataFunctor());
		k = 0;
		for (vector<Rena>::const_iterator it = animal.end() - compare;
			it != animal.end(); ++it){
			k++;
			cout << k << " - "<< it->name << endl;
		}
		animal.clear();

	}
	return 0;
}