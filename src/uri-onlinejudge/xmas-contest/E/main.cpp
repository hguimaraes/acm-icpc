#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

int main(int argc, char **argv){

	string word[] = {
		"Feliz Natal!",
        "Frohliche Weihnachten!",
        "Frohe Weihnacht!",
        "Chuk Sung Tan!",
        "Feliz Navidad!",
        "Kala Christougena!",
        "Merry Christmas!",
        "Merry Christmas!",
        "Merry Christmas!",
        "Feliz Natal!",
        "God Jul!",
        "Mutlu Noeller",
        "Feliz Navidad!",
        "Feliz Navidad!",
        "Feliz Navidad!",
        "Merry Christmas!",
        "Merry Christmas!",
        "Nollaig Shona Dhuit!",
        "Zalig Kerstfeest!",
        "Buon Natale!",
        "Buon Natale!",
        "Milad Mubarak!",
        "Milad Mubarak!",
        "Merii Kurisumasu!",
	};

	string country[] = {
		"brasil",
		"alemanha",
		"austria",
		"coreia",
		"espanha",
		"grecia",
		"estados-unidos",
		"inglaterra",
		"australia",
		"portugal",
		"suecia",
		"turquia",
		"argentina",
		"chile",
		"mexico",
		"antardida",
		"canada",
		"irlanda",
		"belgica",
		"italia",
		"libia",
		"siria",
		"marrocos",
		"japao"
	};

	string enter;

	while(cin >> enter){
		for (int i = 0; i < 24; ++i){
			if (enter == country[i]) {
				cout << word[i] << endl;
				break;
			}
			else if (enter != country[i] && i == 23) cout << "--- NOT FOUND ---" << endl;
		}
	}

	return 0;
}