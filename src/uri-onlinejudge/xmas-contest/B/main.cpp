#include <cstdio>
#include <cmath>

int main(int argc, char **argv){
	int value;

	while (scanf("%d",&value) != EOF) {
		printf("%.2lf\n",2*value*value*(sqrt(3)/5));
	}
	return 0;
}