#include <cstdio>
#include <cmath>

int main(int argc, char **argv){

	int N = 1;
	int Q;
	double A,B;
	while (N != 0){
		scanf("%d",&N);
		for (int i = 0; i < N; ++i){
			scanf("%d%lf%lf",&Q,&A,&B);
			printf("Size #%i:\nIce Cream Used: %.2lf cm2\n",i+1,Q*5*(A+B)/2);
			if(i == (N-1)) printf("\n");
		}
	}
	return 0;
}