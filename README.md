## ACM-ICPC


### Introduction

This repository contain all the problem that I solved from some websites to trainning for programming competitions. It's important to say this are not official answers for the problems, I just solved for fun and I hope it can help someone!

All the codes here are free the use, but I recommend you to not copy them. First of all because I'm not a expert in Programming competitions and I'm sure there are other elegant solutions on the web.

Most part of my code will be in C/C++, but if I see an oportunity to use a different (Hippie!) Language and solve more quickly a problem, I will try.

Cheers!