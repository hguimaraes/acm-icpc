#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

template<class T>
void manualmerge(int p, int q , int r ,vector<T>& array)
{
	int i,j,k;
	T *w = new T[r-p];

	i = p;
	j = q;
	k = 0;

	for (i = 0, k = p; k < q; ++i, ++k) w[i] = array[k];
   	for (j = r-p-1, k = q; k < r; --j, ++k) w[j] = array[k];
   	
   	i = 0; j = r-p-1;
   	
   	for (k = p; k < r; ++k)
      if (w[i] <= w[j]) array[k] = w[i++];
      else array[k] = w[j--];

	delete []w;

}

template<class T>
void mergesort(int p, int r, vector<T>& array)
{
	if (p < r-1) {
      int q = (p + r)/2;

      mergesort( p, q, array);
      mergesort( q, r, array);
      manualmerge(p, q, r, array);

   }
}

template<class T>
vector<T> STLmergesort(vector<T>& array)
{
   if (array.size() <= 1)
      return array;

	int middle = array.size() / 2;
   vector<T> left(array.begin(), array.begin()+middle);
   vector<T> right(array.begin()+middle, array.end());
 
   left = STLmergesort(left);
   right = STLmergesort(right);
 
   vector<T> result(array.size());
   std::merge(left.begin(), left.end(), 
              right.begin(), right.end(),
              result.begin());

   return result;
}