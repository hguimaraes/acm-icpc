/*

////////////////////////////
//     Insertion Sort    //
//////////////////////////

Is intuitive algorithm to ordenate an array based in a movement to ordenate cards.
You can find more details about in the Cormen's book.
The algorithm has a complexity of O(n^2).

*/
#include <iostream>
#include <vector>
#include <chrono>

using namespace std;

template <class T>
void insertionsort(vector<T>& array)
{
	int key,i;
	
	cout << " --   Insertion Sort Algorithm" << endl;
	cout << " ---  Starting the timer and using the Insertion sort Algorithm" 
	<< endl << endl;

	for (int j = 1; j < array.size(); ++j)
	{
		key = array[j];
		// Insert array[j] into the sorted sequence array[0 ..size-1]

		i = j - 1;
		while (i >= 0 && array[i] > key){
			array[i+1] = array[i];
			i -= 1;
		}

		array[i+1] = key;
	}
}

template <class T>
void linearsearch(vector<T>& array, T value){
	int ret = -1;

	cout << " --   Linear search Algorithm" << endl;
	cout << " ---  Starting the timer and using the Linear search sort Algorithm" 
	<< endl << endl;

	for (int i = 0; i < array.size(); ++i)
	{
		if (array[i] == value) {
			ret = i;
			i = array.size();
			break;
		}
	}

	(ret != -1) ? cout << "Found at " << ret << " position" << endl : 
				  cout << "Not found." << endl << endl;
}