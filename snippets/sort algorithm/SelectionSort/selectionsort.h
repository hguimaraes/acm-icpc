/*

////////////////////////////
//     Selection Sort    //
//////////////////////////

Is intuitive algorithm to ordenate an array. You can find more details
about in the Cormen's book.
The algorithm has a complexity of O(n^2).

*/

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

template<class T>
void selectionsort(vector<T>& array)
{
	int min, aux;

	cout << " --   Selection Sort Algorithm" << endl;
	cout << " ---  Starting the timer and using the Selection Sort Algorithm" 
	<< endl << endl;

  	for (int i = 0; i < array.size() - 1; i++) 
   	{
    	min = i;
    	for (int j = (i+1); j < array.size(); j++) {
      		if(array[j] < array[min]) {
        	min = j;
      		}
    	}

    	if (i != min) {
      		aux = array[i];
      		array[i] = array[min];
      		array[min] = aux;
    	}
  	}
}


template<class T>
void STLselectionsort( vector<T>& array )
{

	cout << " --   Selection Sort Algorithm" << endl;
	cout << " ---  Starting the timer and using the Selection Sort Algorithm" 
	<< endl << endl;

  	for(typename std::vector<T>::iterator it = array.begin(); it != array.end()-1; ++it )
  		std::iter_swap(it, std::min_element(it, array.end()));
}